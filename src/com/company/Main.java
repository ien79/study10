package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.company.Const.COUNT;
import static com.company.Const.WORD;

public class Main {

    public static void main(String[] args) {
	// write your code here

        List<String> out = new ArrayList<>(COUNT);
        Scanner sc = new Scanner(System.in);

        Pattern pattern = Pattern.compile(WORD);
        Matcher matcher = null;

        for (int i = 0; i < COUNT; i++) {
            String str = sc.nextLine();
// третий вариант
            matcher = pattern.matcher(str);
            if (matcher.find()){
                out.add(str);
            }


// второй вариант
//            String[] words;
//            words = str.split(" ");
//            for (int j = 0; j < words.length; j++) {
//                if (words[j].equals(WORD)){
//                    out.add(str);
//                    break;
//                }
//            }

// первый вариант
//            if (str.contains(WORD)){
//                out.add(str);
//            }
        }

        String fileOut = "d:\\out12.txt";
        Utils.writeToFile(fileOut, out);

    }
}
