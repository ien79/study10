package com.company;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;

import static com.company.Const.*;

public class Utils {


    public static List<String> readFromFile(String fileName) {
        List<String> lines = new ArrayList<>();
        try {
            lines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);

            // удаление маркера BOM UTF8 из первой строки
            String tmp = lines.get(0);
            tmp = tmp.replaceFirst("^\uFEFF","");
            lines.remove(0);
            lines.add(0, tmp);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }

    public static void writeToFile(String fileName, List<String> lines) {
        FileWriter writer;
        try {
            writer = new FileWriter(fileName, false);
            for (String str : lines) {
                writer.write(str + "\n");
            }
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getRuAlphaPosition(char ch) {
        for (int i = 0; i < RU_alpha.length(); i++) {
            if (ch == RU_alpha.charAt(i)) {
                if (i + 1 < 10) {
                    return "  " + (i + 1);
                } else return " " + (i + 1);
            }
        }
        return " ";
    }

    public static boolean isRuAlpha(char ch) {
        for (int i = 0; i < RU_alpha.length(); i++) {
            if (ch == RU_alpha.charAt(i)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isRuGlasn(char ch) {
        for (int i = 0; i < ru_glasn.length; i++) {
            if (ch == ru_glasn[i]) {
                return true;
            }
        }
        return false;
    }

    public static boolean isRuSoglasn(char ch) {
        for (int i = 0; i < ru_soglasn.length; i++) {
            if (ch == ru_soglasn[i]) {
                return true;
            }
        }
        return false;
    }

    public static boolean isZnak(char ch) {

        for (int i = 0; i < strZnaki.length; i++) {
            if (ch == strZnaki[i]) {
                return true;
            }
        }
        return false;
    }

    public static TreeMap<String, Integer> getMapFromListByRegExpr(List<String> lines, String regExp) {

        TreeMap<String, Integer> map = new TreeMap<>();
        String[] words;
        Integer val;

        for (String line : lines) {
            words = line.split(regExp);

            for (String word : words) {
                word = word.toLowerCase();
                if (word.length() > 0) {
                    if (map.containsKey(word)) {
                        val = map.get(word);
                        map.put(word, ++val);
                    } else {
                        map.put(word, 1);
                    }
                }
            }
        }
        return map;
    }

    public static TreeSet<String> getSetFromListByRegExpr(List<String> lines, String regExp) {

        TreeSet<String> set = new TreeSet<>();
        String[] words;

        for (String line : lines) {
            words = line.split(regExp);

            for (String word : words) {
                word = word.toLowerCase();
                if (word.length() > 0) {
                    set.add(word);
                }
            }
        }
        return set;
    }

}
