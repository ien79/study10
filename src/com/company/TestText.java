package com.company;

import org.junit.Test;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.company.Const.*;


public class TestText {
/*
Считать текст из файла текст.txt
Результат работы программы записать в файл outn.txt где n - это номер задания
Задания должны быть оформлены в виде отдельных тестовых классов. Каждая задача это отдельный метод с аннотацией @Test (внутри этого метода могут вызываться вспомогательные)
*/

    @Test
    public void testReadFromFile() {
        List<String> lines;


        lines = Utils.readFromFile(fileName);
        String[] words;
        for (String line : lines) {
            words = line.split(wordRegExp);
            for (String word : words) {
                for (char ch : word.toCharArray()) {
                    char aaa = ch;
                }
            }
        }
    }

    @Test
    public void testWriteToFile() {
        List<String> lines;
        String fileOut = "d:\\outn.txt";
        lines = Utils.readFromFile(fileName);
        Utils.writeToFile(fileOut, lines);
    }

    @Test
    public void task01() {
// 1. В каждом слове текста 6-ю букву заменить заданным символом. Если в слове более 6 букв, корректировку не выполнять.
        List<String> lines;
        List<String> out = new ArrayList<>();
        lines = Utils.readFromFile(fileName);

        String toReplace = "*";
        String newWord;
        String newLine;
        String[] words;
        for (String line : lines) {
            words = line.split(wordRegExp);
            newLine = line;
            for (String word : words) {
                if (word.length() == 6) {
                    newWord = word.substring(0, 5) + toReplace;
                    newLine = newLine.replaceFirst(word, newWord);
                }
            }
            out.add(newLine);
        }
        String fileOut = "d:\\out01.txt";
        Utils.writeToFile(fileOut, out);
    }

    @Test
    public void task02() {
//        2.В тексте каждую букву заменить ее порядковым номером в алфавите. При
//        выводе в одной строке печатать текст с двумя пробелами между буквами,
//                в следующей строке внизу под каждой буквой печатать ее номер.

        List<String> lines;
        List<String> out = new ArrayList<>();
        lines = Utils.readFromFile(fileName);

        for (String line : lines) {
            StringBuilder str1 = new StringBuilder();
            StringBuilder str2 = new StringBuilder();
            for (int i = 0; i < line.length(); i++) {
                char charLower = line.toLowerCase().charAt(i);
                if (Utils.isRuAlpha(charLower)) {
                    str1.append("  ");
                    str1.append(line.charAt(i));
                    str2.append(Utils.getRuAlphaPosition(charLower));
                } else {
                    str1.append(line.charAt(i));
                    str2.append(" ");
                }
            }
            out.add(str1.toString());
            out.add(str2.toString());
        }
        String fileOut = "d:\\out02.txt";
        Utils.writeToFile(fileOut, out);
    }

    @Test
    public void task03() {
//        3.Из текста удалить все символы, кроме пробелов, не являющиеся буквами. Между
//        последовательностями подряд идущих букв оставить хотя бы один пробел.
        List<String> lines;
        List<String> out = new ArrayList<>();
        lines = Utils.readFromFile(fileName);
        String[] words;
        for (String line : lines) {
            words = line.split(wordRegExp);

            StringBuilder newStr = new StringBuilder();

            for (String word : words) {
                newStr.append(word);
                newStr.append(" ");
            }
            out.add(newStr.toString());
        }
        String fileOut = "d:\\out03.txt";
        Utils.writeToFile(fileOut, out);
    }

    @Test
    public void task04() {
//        4.Определить, сколько раз повторяется в тексте каждое слово, которое встре-
//                чается в нем.

        List<String> lines;
        List<String> out = new ArrayList<>();

        Map<String, Integer> map;

        lines = Utils.readFromFile(fileName);
        map = Utils.getMapFromListByRegExpr(lines, wordRegExp);

        for (Map.Entry<String, Integer> me : map.entrySet()) {
            out.add(me.getKey() + "(" + me.getValue() + ")");
        }
        String fileOut = "d:\\out04.txt";
        Utils.writeToFile(fileOut, out);
    }

    @Test
    public void task05() {
//        5.В тексте найти и напечатать 5 символов (и их количество), встречающихся
//        наиболее часто.

        int maxCnt = 5;

        List<String> lines;
        List<String> out = new ArrayList<>();
        List<String> out2 = new ArrayList<>();

        Map<String, Integer> map;
        Map<Integer, String> map2 = new TreeMap<>();

        lines = Utils.readFromFile(fileName);
        map = Utils.getMapFromListByRegExpr(lines, allSymbRegExp);

        for (Map.Entry<String, Integer> me : map.entrySet()) {
            map2.put(me.getValue(), me.getKey());
        }

        for (Map.Entry<Integer, String> me : map2.entrySet()) {
            out2.add(me.getKey() + "(" + me.getValue() + ")");
        }

        for (int i = out2.size() - 1; i != out2.size() - (1 + maxCnt); i--) {
            out.add(out2.get(i));
        }

        String fileOut = "d:\\out05.txt";
        Utils.writeToFile(fileOut, out);
    }

    @Test
    public void task06() {
//        6. Напечатать без повторения слова текста, у которых первая и последняя бук-
//                вы совпадают.
        List<String> lines;

        TreeSet<String> set;

        lines = Utils.readFromFile(fileName);
        set = Utils.getSetFromListByRegExpr(lines, wordRegExp);
        List<String> out = new ArrayList<>(set);
        for (int i = out.size() - 1; i != -1; i--) {
            String str = out.get(i);
            if (str.charAt(0) != str.charAt(str.length() - 1)) {
                out.remove(i);
            }
        }

        String fileOut = "d:\\out06.txt";
        Utils.writeToFile(fileOut, out);
    }

    @Test
    public void task07() {
//        7. Найти, каких букв, гласных или согласных, больше в каждом предложении
//        текста.
        List<String> lines;
        List<String> out = new ArrayList<>();

        lines = Utils.readFromFile(fileName);
        String[] words;

        for (String line : lines) {
            words = line.split(pointRegExp);
            for (String word : words) {
                Integer cntGlasn = 0;
                Integer cntSoglasn = 0;
                for (char ch : word.toLowerCase().toCharArray()) {
                    if (Utils.isRuGlasn(ch)) {
                        cntGlasn++;
                    } else if (Utils.isRuSoglasn(ch)) {
                        cntSoglasn++;
                    }
                }
                if (cntGlasn > 0 || cntSoglasn > 0) {
                    out.add("гласных=" + cntGlasn + " согласных=" + cntSoglasn + " : " + word.trim());
                }
            }
        }
        String fileOut = "d:\\out07.txt";
        Utils.writeToFile(fileOut, out);
    }

    @Test
    public void task08() {
//        8. Подсчитать количество содержащихся в данном тексте знаков препинания.


        List<String> lines;
        List<String> out = new ArrayList<>();

        Map<String, Integer> map = new TreeMap<>();

        lines = Utils.readFromFile(fileName);
        String[] words;
        Integer val;

        for (String line : lines) {
            words = line.split(allSymbRegExp);

            for (String word : words) {
                if (word.length() > 0 && Utils.isZnak(word.charAt(0))) {
                    if (map.containsKey(word)) {
                        val = map.get(word);
                        map.put(word, ++val);
                    } else {
                        map.put(word, 1);
                    }
                }
            }
        }

        int cnt = 0;
        for (Map.Entry<String, Integer> me : map.entrySet()) {
            out.add(me.getKey() + "(" + me.getValue() + ")");
            cnt = cnt + me.getValue();
        }
        out.add("Итого = " + cnt);
        String fileOut = "d:\\out08.txt";
        Utils.writeToFile(fileOut, out);
    }

    @Test
    public void task09() {
//        9.Вывести в заданном тексте все слова, расположив их в алфавитном порядке.
        List<String> lines;

        TreeSet<String> set;

        lines = Utils.readFromFile(fileName);
        set = Utils.getSetFromListByRegExpr(lines, wordRegExp);
        List<String> out = new ArrayList<>(set);

        String fileOut = "d:\\out09.txt";
        Utils.writeToFile(fileOut, out);
    }

    @Test
    public void task10() {
//        10. Преобразовать текст так, чтобы только первые буквы каждого предложения
//        были заглавными.
        List<String> lines;
        List<String> out = new ArrayList<>();
        lines = Utils.readFromFile(fileName);
        String[] words;
        String[] sentence;

        for (String line : lines) {
            sentence = line.split(pointRegExp);
            for (String sent : sentence) {
                StringBuilder sentNew = new StringBuilder();
                if (!sent.isEmpty()) {
                    sent = sent.trim().toLowerCase();
                    words = sent.split(wordRegExp);
                    if (!words[0].isEmpty()) {
                        sentNew.append(words[0].toUpperCase().substring(0, 1));
                        sentNew.append(words[0].toLowerCase().substring(1, words[0].length()));
                        sentNew.append(sent.substring(sentNew.length(), sent.length()));
                        out.add(sentNew.toString());
                    }
                }
            }
        }
        String fileOut = "d:\\out10.txt";
        Utils.writeToFile(fileOut, out);
    }

    @Test
    public void task11() {
        List<String> lines;
        List<String> out = new ArrayList<>();
        String[] words;
        lines = Utils.readFromFile(fileName);

        for (String line : lines) {
            line = line.trim();
            StringBuilder str = new StringBuilder();
            words = line.split(" ");
            str.append(words[words.length - 1]);
            str.append(line.trim().substring(words[0].length(), line.length() - words[words.length - 1].length()));
            str.append(words[0]);
            out.add(str.toString());
        }

        String fileOut = "d:\\out11.txt";
        Utils.writeToFile(fileOut, out);
    }

    @Test
    public void task13() {
//        3. Есть текстовый файл со списком email. Отсортировать валидные и не валидные email по 2м файлам
        // до @ не более 10 символов
        // n символов + . + не более 5 символов
        List<String> lines;
        List<String> outOk = new ArrayList<>();
        List<String> outBad = new ArrayList<>();
        String[] words;
        lines = Utils.readFromFile(fileName2);

        Pattern pattern = Pattern.compile("\\w{1,10}@\\w+\\.[A-Za-z]{2,5}");
        Matcher matcher = null;

        for (String line : lines) {
            matcher = pattern.matcher(line);
            if (matcher.matches()) {
                outOk.add(line);
            } else outBad.add(line);
        }

        String fileOutOk = "d:\\out13Ok.txt";
        Utils.writeToFile(fileOutOk, outOk);
        String fileOutBad = "d:\\out13Bad.txt";
        Utils.writeToFile(fileOutBad, outBad);
    }

    @Test
    public void task14() {
//        4. Прочитать текст Java-программы и все слова public в объявлении атрибу-
//                тов и методов класса заменить на слово private.
        List<String> lines;
        List<String> out = new ArrayList<>();
        lines = Utils.readFromFile(fileName3);
        for (String line : lines) {
            out.add(line.replaceFirst("public", "private"));
        }
        String fileOut = "d:\\out14.txt";
        Utils.writeToFile(fileOut, out);
    }

    @Test
    public void task015() {
//        5. В файле, содержащем фамилии студентов и их оценки, записать прописны-
//                ми буквами фамилии тех студентов, которые имеют средний балл более 7.
        List<String> lines;
        List<String> out = new ArrayList<>();
        lines = Utils.readFromFile(fileName4);
        String[] words;
        for (String line : lines) {
            words = line.trim().split(" ");

            double sum = 0;
            for (int i = 1; i < words.length; i++) {
                sum = sum + Double.parseDouble(words[i]);
            }

            if (sum/(words.length-1) > AVERAGE){
                out.add(line.replaceFirst(words[0],words[0].toUpperCase()));
            }else out.add(line);
        }
        String fileOut = "d:\\out15.txt";
        Utils.writeToFile(fileOut, out);
    }
}
