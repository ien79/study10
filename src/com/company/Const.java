package com.company;

public class Const {

    public static final char[] ru_glasn = new char[]{'а', 'я', 'у', 'ю', 'и', 'ы', 'э', 'е', 'о', 'ё'};
    public static final char[] ru_soglasn = new char[]{'б', 'в', 'г', 'д', 'ж', 'з', 'й', 'к', 'л', 'м', 'н', 'п', 'р', 'с', 'т', 'ф', 'х', 'ц', 'ч', 'ш', 'щ'};

    public static final String RU_alpha = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";

    public static final String fileName = "d:\\текст.txt";
    public static final String fileName2 = "d:\\mail.txt";
    public static final String fileName3 = "d:\\study\\java\\Study10\\src\\com\\company\\TestText.java";
    public static final String fileName4 = "d:\\текст2.txt";

    public static final String wordRegExp = "[\\'\\:\\!\\)\\(\\?;\\-,\\.\\s]+";
    public static final String allSymbRegExp = "";
    public static final String pointRegExp = "[\\.\\!\\?]+";
    public static final char[] strZnaki = {'.', ',', ':', ';', '!', '?', '-', '(', ')', '"'};

    public static final String WORD = "Softclub";
    public static final int COUNT = 5;

    public static final double AVERAGE = 7.0;
}
